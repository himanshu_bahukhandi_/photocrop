package com.paytm.photocrop.model;

/**
 * Created by himanshu.bahukhandi on 26/10/17.
 */
//This model will contain ids which are needed by cropping activity like cropview and button to preview or next.
public class CropLayoutIdModel {

    int cropView;
    int photoView;
    int doneButton;
    int previewButton;
    int cancelButton;

    // pass 0 whichever button you dont have in your crop layout but cropView and photoView is manadatory.
    public CropLayoutIdModel(int cropView, int photoView, int doneButton, int cancelButton,int previewButton) {
        this.cropView = cropView;
        this.photoView = photoView;
        this.doneButton = doneButton;
        this.previewButton = previewButton;
        this.cancelButton = cancelButton;
    }

    public int getCropView() {
        return cropView;
    }

    public void setCropView(int cropView) {
        this.cropView = cropView;
    }

    public int getPhotoView() {
        return photoView;
    }

    public void setPhotoView(int photoView) {
        this.photoView = photoView;
    }

    public int getDoneButton() {
        return doneButton;
    }

    public void setDoneButton(int doneButton) {
        this.doneButton = doneButton;
    }

    public int getPreviewButton() {
        return previewButton;
    }

    public void setPreviewButton(int previewButton) {
        this.previewButton = previewButton;
    }

    public int getCancelButton() {
        return cancelButton;
    }

    public void setCancelButton(int cancelButton) {
        this.cancelButton = cancelButton;
    }
}
