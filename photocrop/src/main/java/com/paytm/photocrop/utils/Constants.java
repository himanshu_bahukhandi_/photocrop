package com.paytm.photocrop.utils;


public class Constants {

    public interface IntentExtras {
        String ACTION_CAMERA = "action-camera";
        String ACTION_GALLERY = "action-gallery";
        String IMAGE_PATH = "image-path";
    }

    public interface PicModes {
        String CAMERA = "Camera";
        String GALLERY = "Gallery";
    }
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
}
