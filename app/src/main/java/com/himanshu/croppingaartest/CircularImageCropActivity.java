package com.himanshu.croppingaartest;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.paytm.photocrop.activity.ImageCropBaseActivity;
import com.paytm.photocrop.model.CropLayoutIdModel;

/**
 * Created by himanshu.bahukhandi on 26/10/17.
 */

public class CircularImageCropActivity extends ImageCropBaseActivity {


    private View retakeButton;
    private TextView previewTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        retakeButton = (View) findViewById(R.id.retakeButton);
        retakeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CircularImageCropActivity.this, "On Retry clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public CropLayoutIdModel getCropModel() {
        return new CropLayoutIdModel(R.id.crop_overlay, R.id.iv_photo, R.id.doneButton, R.id.cancelBtn, R.id.previewButton);
    }

    @Override
    public int getLayoutId() {
        return R.layout.abc;
    }

    @Override
    public void onPreviewClick(Bitmap mImagePath) {


        // Toast.makeText(CircularImageCropActivity.this,mImagePath, Toast.LENGTH_SHORT).show();
        //Do whatever with this image; swap layout with BankCardViewFragment

    }

    @Override
    public void onDoneClick(String mImagePath) {
        //Toast.makeText(CircularImageCropActivity.this,mImagePath, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCancelClick() {
        Toast.makeText(CircularImageCropActivity.this, "Cancel Clicked", Toast.LENGTH_SHORT).show();
    }
}
